//COMMENTS in Javascript
// - to write comments, we use forward slash for single line comments and two forward slash and two asterisks for multi line comment.
//This is a single line comment
/*
This is a multi line comment.
To write them, we add two asterisks inside of the forward slashes and wrote comments in between line.
*/

// 	VARIABLE
/*
	- Variables contain values that can be changed over the execution of time of the program.
	- To declare a variable, we use the "let" keyword.
*/

let productName = 'desktop computer';
console.log(productName)
productName="cellphone"
console.log(productName)
let productPrice = 500;
console.log(productPrice)
productPrice = 450;
console.log(productPrice)

// CONTANTS
/*
	- Use constants for values that will not change
*/

// DATA TYPES

// 1. STRINGS
/*
	- strings or a series of characters that create a word, a phrase, sentence or anything related to "TEXT"
	- strings in Javascript can be written using a s.
*/


// OBJECTS

	// Arrays
	/* - They are special kind of data that stores multiple values,
		- They are a special type of object
		- They can store different data types but is normally used to store similar data types.
	*/

	// Syntax
		// Let/const arrayName = [ElementA, ElementB, ElementC]


	let grades=[98.7,97.1,90.2,94.6]
	console.log(grades)
	console.log(grades[0])

	let userDetails=["John","Smith",32,true]
	console.log(userDetails);

	// Object Literals
	/*
		- Objects are another special kind of data type that mimics real world objects/items
		- They are used to create complex data that contains pieces of information that are relevant to each other.
		- Every individual piece of information if called property of the object.

		Syntax:

			let/const objectName = (
			propertyA:value;
			propertyB:value;
			)
	*/


	let personDetails = {
		fullName: "Juan Dela Cruz",
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
			}
	}



















