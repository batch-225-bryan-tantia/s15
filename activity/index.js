let firstName = 'Bryan'
console.log("First Name: " + firstName)
let lastName = "Tantia"
console.log	("Last Name: " + lastName)
let age = 20
console.log("Age: " + age)

let hobbies = ['reading','watching movies','playing video games']
console.log("Hobbies: ")
console.log(hobbies)

let workAddress = {
	houseNumber:'32', 
	street:"Washington", 
	city:"Lincoln", 
	state:"Nebraska"
}
console.log("Work Address: ")
console.log(workAddress)

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let ageNew = 40;
	console.log("My current age is: " + ageNew);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullNames = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	lastLocations = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);